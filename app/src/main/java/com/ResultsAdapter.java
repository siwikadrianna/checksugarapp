package com;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.newtry.R;


import java.util.List;

public class ResultsAdapter extends ArrayAdapter<Results> {

    private String unit;
    private Activity context;
    private List<Results> resultsList;
    private TextView tUnit;

    public ResultsAdapter(Activity context, List<Results> resultsList, String unit) {
        super(context, R.layout.list_layout, resultsList);
        this.context = context;
        this.resultsList = resultsList;
        this.unit = unit;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = inflater.inflate(R.layout.list_layout, null, true);

        TextView date = listViewItem.findViewById(R.id.tvDate);
        TextView result = listViewItem.findViewById(R.id.tvRes);
        TextView mood = listViewItem.findViewById(R.id.tvMood);
        TextView time = listViewItem.findViewById(R.id.tvTime);
        tUnit = listViewItem.findViewById(R.id.tvUnit);

        Results results = resultsList.get(position);
        date.setText(results.getDate());
        result.setText(results.getResult());
        mood.setText(results.getMood());
        time.setText(results.getTime());

        setUnit(unit);

        return listViewItem;
    }

    public void setUnit(String ChekedState) {
        if (ChekedState.matches("2")) {
            tUnit.setText("mmol/l");
        } else {
            tUnit.setText("mg/dl");
        }
    }
}

