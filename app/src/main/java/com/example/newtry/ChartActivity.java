package com.example.newtry;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.protobuf.StringValue;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

public class ChartActivity extends AppCompatActivity {
    private Date currentDate;
    private ArrayList<String> myListResult = null;
    private ArrayList<String> myListDate = null;
    private String unit;
    private String monthName, minText, maxText;
    private TextView tvMax, tvMin;
    private double min, max;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onStart() {
        super.onStart();
        loadData();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);

        tvMin = findViewById(R.id.tvMin);
        tvMax = findViewById(R.id.tvMax);
        currentDate = new Date();
        Locale defLoc = Locale.getDefault();
        SimpleDateFormat formatterDay = new SimpleDateFormat("MMMM", defLoc);
        monthName = formatterDay.format(currentDate);

        unit = getIntent().getStringExtra("unit");

        loadData();


    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void loadData() {
        myListDate = null;
        myListResult = null;
        myListResult = getIntent().getStringArrayListExtra("resLis");
        myListDate = getIntent().getStringArrayListExtra("resDate");

        int[] day = new int[myListDate.size()];
        int[] month = new int[myListDate.size()];
        int[] year = new int[myListDate.size()];
        int[] resCounted = getIntent().getIntArrayExtra("resCounted");

        assert myListResult != null;
        int[] resultsTab1 = new int[myListResult.size()];
        double[] resultsTab2 = new double[myListResult.size()];
        assert myListDate != null;
        String[] fullDate = new String[myListDate.size()];
        String[] splitDate = new String[myListDate.size()];


        PieChart pieChart = findViewById(R.id.PieChart);
        ArrayList<PieEntry> res = new ArrayList<>();
        BarChart barChart = findViewById(R.id.barChart);
        ArrayList<BarEntry> results = new ArrayList<>();

        PieDataSet pieDataSet = new PieDataSet(res, " ");
        pieDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        pieDataSet.setValueTextColor(Color.BLACK);
        pieDataSet.setValueTextSize(16f);

        for (int i = 0; i < myListResult.size(); i++) {
            System.out.println("Wynik z listy: " + myListResult.get(i));
            System.out.println("Data z listy: " + myListDate.get(i));
            fullDate[i] = myListDate.get(i);
            splitDate[i] = fullDate[i].replace(".", "");
            day[i] = Integer.parseInt(splitDate[i].substring(6, 8));
            month[i] = Integer.parseInt(splitDate[i].substring(4, 6));
            year[i] = Integer.parseInt(splitDate[i].substring(0, 4));
            if (unit.matches("2")) {
                resultsTab2[i] = Double.parseDouble(myListResult.get(i).replace(",", "."));
                results.add(new BarEntry(day[i], (float) resultsTab2[i]));
            } else {
                resultsTab1[i] = Integer.parseInt(myListResult.get(i));

                results.add(new BarEntry(day[i], resultsTab1[i]));

            }
        }

        if (unit.matches("2")) {
            min = Arrays.stream(resultsTab2).min().getAsDouble();
            max = Arrays.stream(resultsTab2).max().getAsDouble();
            String min1 = String.valueOf(min).replace(".", ",");
            String max1 = String.valueOf(max).replace(".", ",");
            minText = "Najmniejszy wynik w tym miesiącu wyniósł: " + min1 + " mmol/l";
            maxText = "Największy wynik w tym miesiącu wyniósł: " + max1 + " mmol/l";
            tvMin.setText(minText);
            tvMax.setText(maxText);
        } else {
            min = Arrays.stream(resultsTab1).min().getAsInt();
            max = Arrays.stream(resultsTab1).max().getAsInt();
            minText = "Najmniejszy wynik w tym miesiącu wyniósł: " + min + " mg/dl";
            maxText = "Największy wynik w tym miesiącu wyniósł: " + max + " mg/dl";
            tvMin.setText(minText);
            tvMax.setText(maxText);
        }

        if (resCounted != null) {
            res.add(new PieEntry(resCounted[0], "Bardzo Dobrze!"));
            res.add(new PieEntry(resCounted[1], "Dobrze"));
            res.add(new PieEntry(resCounted[2], "Żle"));
        }

        PieData pieData = new PieData(pieDataSet);

        pieChart.setData(pieData);
        pieChart.setCenterText("WYNIKI ZE \r\n" + monthName.toUpperCase());
        pieChart.getDescription().setEnabled(false);
        pieChart.animate();


        BarDataSet barDataSet = new BarDataSet(results, " ");
        barDataSet.setColors(ColorTemplate.MATERIAL_COLORS);
        barDataSet.setBarBorderColor(Color.BLACK);
        barDataSet.setBarBorderWidth(0.5f);
        barDataSet.setValueTextColors(Collections.singletonList(Color.BLACK));
        barDataSet.setValueTextSize(10f);


        BarData barData = new BarData(barDataSet);
        barChart.setFitBars(true);
        barChart.setData(barData);
        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        YAxis yAxis = barChart.getAxisRight();
        yAxis.setEnabled(false);
        barChart.getDescription().setEnabled(false);
        barChart.animateY(2000);

    }
}
