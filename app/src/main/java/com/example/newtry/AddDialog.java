package com.example.newtry;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.Results;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.w3c.dom.Text;

import java.security.PrivateKey;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AddDialog extends DialogFragment {
    private static final String TAG = "AddDialog";

    private EditText mInput;
    private TextView mActionOk, mActionCancel, Time;
    private DatePicker picker;
    private TimePicker pickerTime;
    private FirebaseFirestore db;
    private RadioButton rbGood, rbNormal, rbBad;
    private String mood;
    private String hours, minutes, userName, unit;
    private double inputI;

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_add, container, false);

        db = FirebaseFirestore.getInstance();
        mActionCancel = view.findViewById(R.id.action_cancel);
        mActionOk = view.findViewById(R.id.action_ok);
        mInput = view.findViewById(R.id.input);
        picker = view.findViewById(R.id.datePicker1);
        picker.setMaxDate(System.currentTimeMillis());
        rbGood = view.findViewById(R.id.rbGood);
        rbNormal = view.findViewById(R.id.rbNormal);
        rbBad = view.findViewById(R.id.rbBad);
        pickerTime = view.findViewById(R.id.timePicker1);
        pickerTime.setIs24HourView(true);
        unit = getArguments().getString("unit");
        if (unit.matches("2")) {
            mInput.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
            mInput.setHint("mmol/l");
        }

        mActionCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: closing dialog");
                getDialog().dismiss();
            }
        });
        mActionOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: capturing input");
                String inputS = mInput.getText().toString();
                if (unit.matches("2")) {
                    inputI = Double.parseDouble(inputS) * 18;
                } else {
                    inputI = Integer.parseInt(inputS);
                }
                String input = String.valueOf(inputI);
                String day = String.format("%02d", picker.getDayOfMonth());
                String month = String.format("%02d", (picker.getMonth() + 1));
                String date = picker.getYear() + "." + month + "." + day;
                hours = String.format("%02d", pickerTime.getHour());
                minutes = String.format("%02d", pickerTime.getMinute());
                String time = hours + ":" + minutes;
                mood = CheckMood();
                userName = getArguments().getString("userName");
                if (!input.matches("")) {
                    CollectionReference dbResults = db.collection("Results");
                    Results results = new Results(
                            input, date, mood, time, userName
                    );
                    dbResults.add(results).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                        }
                    });
                    //((MainActivity) getActivity()).mInputDisplay.setText(date + "\n" + input + "\n" + ((MainActivity) getActivity()).mInputDisplay.getText());
                } else {
                    getDialog().dismiss();
                }
                getDialog().dismiss();
            }
        });
        return view;
    }

    private String CheckMood() {
        if (rbGood.isChecked()) {
            mood = "Bardzo dobrze!";
        } else if (rbNormal.isChecked()) {
            mood = "Dobrze";
        } else {
            mood = "Żle";
        }
        return mood;
    }
}
