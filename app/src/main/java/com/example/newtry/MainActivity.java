package com.example.newtry;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.preference.Preference;
import androidx.preference.PreferenceManager;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.Results;
import com.ResultsAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Nullable;

import static java.lang.Double.parseDouble;

public class MainActivity extends AppCompatActivity {


    private Date currentDate;
    private String resultDay, resultMonthYear, resultFullDate, resultDay0;
    SimpleDateFormat formatterDay, formatterMonthYear, formatterFullDate;
    private Button buttonAdd;
    private Button buttonEmeCall;
    private Button buttonSettings, buttonChart;
    private static final String TAG = ".MainActivity";

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    private DatabaseReference dbResults;
    private ListView lvResultsList;
    private List<Results> listResults;
    ArrayList<String> myListDate = new ArrayList<String>();
    ArrayList<String> myListResult = new ArrayList<String>();
    int[] countResults = new int[3];
    private int veryGood, good, bad;
    private String diabType, unit, username;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.LogOut:
                FirebaseAuth.getInstance().signOut();
                int check = 1;
                Intent intSignOut = new Intent(MainActivity.this, SignInActivity.class);
                intSignOut.putExtra("signOut", check);
                startActivity(intSignOut);
                finish();
                break;
        }
        return true;
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);

        unit = pref.getString(SettingsActivity.KEY_PREF, "@string/mg");
        diabType = pref.getString("ms_diabType", "@string/type1");
        bad = good = veryGood = 0;
        loadData();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);

        unit = pref.getString(SettingsActivity.KEY_PREF, "@string/mg");
        diabType = pref.getString("ms_diabType", "@string/type1");

        bad = good = veryGood = 0;
        setTime();

        Intent intent = getIntent();
        username = intent.getStringExtra("name1");

        dbResults = FirebaseDatabase.getInstance().getReference("Results");

        lvResultsList = findViewById(R.id.lvResults);
        listResults = new ArrayList<>();

        loadData();

        // System.out.println("--------------------" + bad + "-----------------------" + good + "-----------------------" + veryGood + "-----------------------");


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        buttonAdd = findViewById(R.id.buttonAdd);

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: opening dialog.");
                AddDialog dialog = new AddDialog();
                Bundle args = new Bundle();
                args.putString("userName", username);
                args.putString("unit", unit);
                dialog.setArguments(args);
                dialog.show(getSupportFragmentManager(), "AddDialog");
            }
        });

        buttonEmeCall = findViewById(R.id.buttonEmeCall);
        buttonEmeCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentCall = new Intent(Intent.ACTION_CALL);
                String emeNumber = "112";
                intentCall.setData(Uri.parse("tel: " + emeNumber));
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(MainActivity.this, "Zezwól na korzystanie z telefonu", Toast.LENGTH_SHORT).show();
                    requestPermission();
                } else {
                    startActivity(intentCall);
                }
            }
        });

        buttonChart = findViewById(R.id.buttonResults);
        buttonChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intChart = new Intent(MainActivity.this, ChartActivity.class);
                intChart.putExtra("resLis", myListResult);
                intChart.putExtra("resDate", myListDate);
                intChart.putExtra("resCounted", countResults);
                intChart.putExtra("unit", unit);
                startActivity(intChart);
            }
        });

        db.collection("Results").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot snapshot, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w(TAG, "Listen failed.", e);
                    return;
                }

                String source = snapshot != null && snapshot.getMetadata().hasPendingWrites()
                        ? "Local" : "Server";

                if (snapshot != null) {
                    Log.d(TAG, source + " data: " + snapshot.getDocumentChanges());
                    loadData();

                } else {
                    Log.d(TAG, source + " data: null");
                }
            }

        });

        buttonSettings = findViewById(R.id.buttonSetting);
        buttonSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SettingsActivity.class));
            }
        });


    }

    private void setTime() {
        currentDate = new Date();
        Locale defLoc = Locale.getDefault();

        formatterDay = new SimpleDateFormat("dd");
        resultDay = formatterDay.format(currentDate);

        formatterMonthYear = new SimpleDateFormat("MMMM yyyy", defLoc);
        resultMonthYear = formatterMonthYear.format(currentDate);

        formatterFullDate = new SimpleDateFormat("yyyy.MM", defLoc);
        resultFullDate = formatterFullDate.format(currentDate);

        resultDay0 = resultFullDate + ".00";

        TextView textViewDay = findViewById(R.id.textViewDay);
        textViewDay.setText(resultDay);

        TextView textViewMonth = findViewById(R.id.textViewMonth);
        textViewMonth.setText(resultMonthYear);

    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CALL_PHONE}, 1);
    }

    public void loadData() {
        db.collection("Results")
                .whereEqualTo("user", username)
                .whereGreaterThan("date", resultDay0)
                .orderBy("date", Query.Direction.DESCENDING)
                .orderBy("time", Query.Direction.DESCENDING)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            listResults.clear();
                            myListResult.clear();
                            myListDate.clear();
                            bad = good = veryGood = 0;
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                //Log.d(TAG, document.getId() + " => " + document.getData());
                                Map<String, Object> tmp = document.getData();
                                Object[] tmpArray = tmp.values().toArray();
                                if (unit.matches("2")) {
                                    String read = tmpArray[1].toString();
                                    DecimalFormat df = new DecimalFormat("##.##");
                                    double d = parseDouble(read) / 18;
                                    String d1 = df.format(d);
                                    System.out.println(tmpArray[0] + " " + d1 + " " + tmpArray[2] + " " + tmpArray[3] + " " + tmpArray[4].toString());
                                    int d11 = (int) Math.round(d * 18);
                                    System.out.println(d11);
                                    myListDate.add(tmpArray[0].toString());
                                    myListResult.add(d1);

                                    if (tmpArray[2].toString().matches("Dobrze")) {
                                        good++;
                                    } else if (tmpArray[2].toString().matches("Żle")) {
                                        bad++;
                                    } else if (tmpArray[2].toString().matches("Bardzo Dobrze!") || tmpArray[2].toString().matches("Bardzo dobrze!")) {
                                        veryGood++;
                                    }

                                    listResults.add(new Results(d1, tmpArray[0].toString(), tmpArray[2].toString(), tmpArray[3].toString(), tmpArray[4].toString()));
                                } else {
                                    System.out.println(tmpArray[0] + " " + tmpArray[1] + " " + tmpArray[2] + " " + tmpArray[3] + " " + tmpArray[4].toString());

                                    String read = tmpArray[1].toString();
                                    double d = parseDouble(read);
                                    int d1 = (int) Math.round(d);
                                    String d11 = String.valueOf(d1);
                                    myListDate.add(tmpArray[0].toString());
                                    myListResult.add(d11);
                                    //System.out.println(d1 + "-----------------------------------------");
                                    if (tmpArray[2].toString().matches("Dobrze")) {
                                        good++;
                                    } else if (tmpArray[2].toString().matches("Żle")) {
                                        bad++;
                                    } else if (tmpArray[2].toString().matches("Bardzo Dobrze!") || tmpArray[2].toString().matches("Bardzo dobrze!")) {
                                        veryGood++;
                                    }
                                    //System.out.println("--------------------" + bad + "-----------------------" + good + "-----------------------" + veryGood + "-----------------------");
                                    listResults.add(new Results(d11, tmpArray[0].toString(), tmpArray[2].toString(), tmpArray[3].toString(), tmpArray[4].toString()));
                                }
                            }
                            countResults[0] = veryGood;
                            countResults[1] = good;
                            countResults[2] = bad;
                            ResultsAdapter adapter = new ResultsAdapter(MainActivity.this, listResults, unit);
                            lvResultsList.setAdapter(adapter);
                            //System.out.println("--------------------" + bad + "-----------------------" + good + "-----------------------" + veryGood + "-----------------------");
                        } else {
                            Log.w(TAG, "Error getting documents.", task.getException());
                        }
                    }

                });
    }
}