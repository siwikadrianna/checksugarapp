package com;

import android.widget.DatePicker;

public class Results {
    private String result, date, time, mood, user;

    public Results(Object date, Object result, Object mood, Object time, Object user) {

    }

    public String getResult() {
        return result;
    }

    public String getMood() {
        return mood;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getUser() {
        return user;
    }

    public Results(String result, String date, String mood, String time, String user) {
        this.result = result;
        this.date = date;
        this.mood = mood;
        this.time = time;
        this.user = user;
    }
}
